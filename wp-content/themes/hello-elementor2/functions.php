<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'HELLO_ELEMENTOR_VERSION', '2.2.0' );

if ( ! isset( $content_width ) ) {
	$content_width = 800; // Pixels.
}

if ( ! function_exists( 'hello_elementor_setup' ) ) {
	/**
	 * Set up theme support.
	 *
	 * @return void
	 */
	function hello_elementor_setup() {
		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_load_textdomain', [ true ], '2.0', 'hello_elementor_load_textdomain' );
		if ( apply_filters( 'hello_elementor_load_textdomain', $hook_result ) ) {
			load_theme_textdomain( 'hello-elementor', get_template_directory() . '/languages' );
		}

		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_menus', [ true ], '2.0', 'hello_elementor_register_menus' );
		if ( apply_filters( 'hello_elementor_register_menus', $hook_result ) ) {
			register_nav_menus( array( 'menu-1' => __( 'Primary', 'hello-elementor' ) ) );
		}

		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_theme_support', [ true ], '2.0', 'hello_elementor_add_theme_support' );
		if ( apply_filters( 'hello_elementor_add_theme_support', $hook_result ) ) {
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'title-tag' );
			add_theme_support(
				'html5',
				array(
					'search-form',
					'comment-form',
					'comment-list',
					'gallery',
					'caption',
				)
			);
			add_theme_support(
				'custom-logo',
				array(
					'height'      => 100,
					'width'       => 350,
					'flex-height' => true,
					'flex-width'  => true,
				)
			);

			/*
			 * Editor Style.
			 */
			add_editor_style( 'editor-style.css' );

			/*
			 * WooCommerce.
			 */
			$hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_woocommerce_support', [ true ], '2.0', 'hello_elementor_add_woocommerce_support' );
			if ( apply_filters( 'hello_elementor_add_woocommerce_support', $hook_result ) ) {
				// WooCommerce in general.
				add_theme_support( 'woocommerce' );
				// Enabling WooCommerce product gallery features (are off by default since WC 3.0.0).
				// zoom.
				add_theme_support( 'wc-product-gallery-zoom' );
				// lightbox.
				add_theme_support( 'wc-product-gallery-lightbox' );
				// swipe.
				add_theme_support( 'wc-product-gallery-slider' );
			}
		}
	}
}
add_action( 'after_setup_theme', 'hello_elementor_setup' );

if ( ! function_exists( 'hello_elementor_scripts_styles' ) ) {
	/**
	 * Theme Scripts & Styles.
	 *
	 * @return void
	 */
	function hello_elementor_scripts_styles() {
		$enqueue_basic_style = apply_filters_deprecated( 'elementor_hello_theme_enqueue_style', [ true ], '2.0', 'hello_elementor_enqueue_style' );
		$min_suffix          = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		if ( apply_filters( 'hello_elementor_enqueue_style', $enqueue_basic_style ) ) {
		    wp_enqueue_style( 'estilos-personalizados', get_template_directory_uri() . '/estilos-personalizados.css',false,'1.1','all');
		    wp_enqueue_script( 'jquery.diyslider', get_template_directory_uri() . '/jquery.diyslider.js', array ( 'jquery' ), 1.1, true);
			wp_enqueue_script( 'script', get_template_directory_uri() . '/script.js', array ( 'jquery' ), 1.1, true);
			wp_enqueue_style(
				'hello-elementor',
				get_template_directory_uri() . '/style' . $min_suffix . '.css',
				[],
				HELLO_ELEMENTOR_VERSION
			);
			
		}

		if ( apply_filters( 'hello_elementor_enqueue_theme_style', true ) ) {
			wp_enqueue_style(
				'hello-elementor-theme-style',
				get_template_directory_uri() . '/theme' . $min_suffix . '.css',
				[],
				HELLO_ELEMENTOR_VERSION
			);
		}
	}
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_scripts_styles' );

if ( ! function_exists( 'hello_elementor_register_elementor_locations' ) ) {
	/**
	 * Register Elementor Locations.
	 *
	 * @param ElementorPro\Modules\ThemeBuilder\Classes\Locations_Manager $elementor_theme_manager theme manager.
	 *
	 * @return void
	 */
	function hello_elementor_register_elementor_locations( $elementor_theme_manager ) {
		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_elementor_locations', [ true ], '2.0', 'hello_elementor_register_elementor_locations' );
		if ( apply_filters( 'hello_elementor_register_elementor_locations', $hook_result ) ) {
			$elementor_theme_manager->register_all_core_location();
		}
	}
}
add_action( 'elementor/theme/register_locations', 'hello_elementor_register_elementor_locations' );

if ( ! function_exists( 'hello_elementor_content_width' ) ) {
	/**
	 * Set default content width.
	 *
	 * @return void
	 */
	function hello_elementor_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'hello_elementor_content_width', 800 );
	}
}
add_action( 'after_setup_theme', 'hello_elementor_content_width', 0 );

if ( is_admin() ) {
	require get_template_directory() . '/includes/admin-functions.php';
}

if ( ! function_exists( 'hello_elementor_check_hide_title' ) ) {
	/**
	 * Check hide title.
	 *
	 * @param bool $val default value.
	 *
	 * @return bool
	 */
	function hello_elementor_check_hide_title( $val ) {
		if ( defined( 'ELEMENTOR_VERSION' ) ) {
			$current_doc = \Elementor\Plugin::instance()->documents->get( get_the_ID() );
			if ( $current_doc && 'yes' === $current_doc->get_settings( 'hide_title' ) ) {
				$val = false;
			}
		}
		return $val;
	}
}
add_filter( 'hello_elementor_page_title', 'hello_elementor_check_hide_title' );

/**
 * Wrapper function to deal with backwards compatibility.
 */
if ( ! function_exists( 'hello_elementor_body_open' ) ) {
	function hello_elementor_body_open() {
		if ( function_exists( 'wp_body_open' ) ) {
			wp_body_open();
		} else {
			do_action( 'wp_body_open' );
		}
	}
}
function cptui_register_my_cpts() {

	/**
	 * Post Type: noticias.
	 */

	$labels = [
		"name" => __( "noticias", "hello-elementor" ),
		"singular_name" => __( "noticia", "hello-elementor" ),
		"menu_name" => __( "Noticias", "hello-elementor" ),
	];

	$args = [
		"label" => __( "noticias", "hello-elementor" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "noticias_portal", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "author" ],
		"taxonomies" => [ "category", "post_tag" ],
	];

	register_post_type( "noticias_portal", $args );

	/**
	 * Post Type: Normativa.
	 */

	$labels = [
		"name" => __( "Normativa", "hello-elementor" ),
		"singular_name" => __( "normativa", "hello-elementor" ),
		"menu_name" => __( "Normativa", "hello-elementor" ),
		"all_items" => __( "Todos los Normativa", "hello-elementor" ),
		"add_new" => __( "Añadir nuevo", "hello-elementor" ),
		"add_new_item" => __( "Añadir nuevo normativa", "hello-elementor" ),
		"edit_item" => __( "Editar normativa", "hello-elementor" ),
		"new_item" => __( "Nueva normativa", "hello-elementor" ),
		"view_item" => __( "Ver normativa", "hello-elementor" ),
		"view_items" => __( "Ver Normativa", "hello-elementor" ),
		"search_items" => __( "Buscar Normativa", "hello-elementor" ),
		"not_found" => __( "No se ha encontrado Normativa", "hello-elementor" ),
		"not_found_in_trash" => __( "No se han encontrado Normativa en la papelera", "hello-elementor" ),
		"parent" => __( "normativa superior", "hello-elementor" ),
		"featured_image" => __( "Imagen destacada para normativa", "hello-elementor" ),
		"set_featured_image" => __( "Establece una imagen destacada para normativa", "hello-elementor" ),
		"remove_featured_image" => __( "Eliminar la imagen destacada de normativa", "hello-elementor" ),
		"use_featured_image" => __( "Usar como imagen destacada de normativa", "hello-elementor" ),
		"archives" => __( "Archivos de normativa", "hello-elementor" ),
		"insert_into_item" => __( "Insertar en normativa", "hello-elementor" ),
		"uploaded_to_this_item" => __( "Subir a normativa", "hello-elementor" ),
		"filter_items_list" => __( "Filtrar la lista de Normativa", "hello-elementor" ),
		"items_list_navigation" => __( "Navegación de la lista de Normativa", "hello-elementor" ),
		"items_list" => __( "Lista de Normativa", "hello-elementor" ),
		"attributes" => __( "Atributos de Normativa", "hello-elementor" ),
		"name_admin_bar" => __( "normativa", "hello-elementor" ),
		"item_published" => __( "normativa publicado", "hello-elementor" ),
		"item_published_privately" => __( "normativa publicado como privado.", "hello-elementor" ),
		"item_reverted_to_draft" => __( "normativa devuelto a borrador.", "hello-elementor" ),
		"item_scheduled" => __( "normativa programado", "hello-elementor" ),
		"item_updated" => __( "normativa actualizado.", "hello-elementor" ),
		"parent_item_colon" => __( "normativa superior", "hello-elementor" ),
	];

	$args = [
		"label" => __( "Normativa", "hello-elementor" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => "normativa",
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "normativa", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "custom-fields", "author" ],
		"taxonomies" => [ "categorias" ],
	];

	register_post_type( "normativa", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts_noticias_portal() {

	/**
	 * Post Type: noticias.
	 */

	$labels = [
		"name" => __( "noticias", "hello-elementor" ),
		"singular_name" => __( "noticia", "hello-elementor" ),
		"menu_name" => __( "Noticias", "hello-elementor" ),
	];

	$args = [
		"label" => __( "noticias", "hello-elementor" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "noticias_portal", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "author" ],
		"taxonomies" => [ "category", "post_tag" ],
	];

	register_post_type( "noticias_portal", $args );
}

add_action( 'init', 'cptui_register_my_cpts_noticias_portal' );
function cptui_register_my_cpts_normativa() {

	/**
	 * Post Type: Normativa.
	 */

	$labels = [
		"name" => __( "Normativa", "hello-elementor" ),
		"singular_name" => __( "normativa", "hello-elementor" ),
		"menu_name" => __( "Normativa", "hello-elementor" ),
		"all_items" => __( "Todos los Normativa", "hello-elementor" ),
		"add_new" => __( "Añadir nuevo", "hello-elementor" ),
		"add_new_item" => __( "Añadir nuevo normativa", "hello-elementor" ),
		"edit_item" => __( "Editar normativa", "hello-elementor" ),
		"new_item" => __( "Nueva normativa", "hello-elementor" ),
		"view_item" => __( "Ver normativa", "hello-elementor" ),
		"view_items" => __( "Ver Normativa", "hello-elementor" ),
		"search_items" => __( "Buscar Normativa", "hello-elementor" ),
		"not_found" => __( "No se ha encontrado Normativa", "hello-elementor" ),
		"not_found_in_trash" => __( "No se han encontrado Normativa en la papelera", "hello-elementor" ),
		"parent" => __( "normativa superior", "hello-elementor" ),
		"featured_image" => __( "Imagen destacada para normativa", "hello-elementor" ),
		"set_featured_image" => __( "Establece una imagen destacada para normativa", "hello-elementor" ),
		"remove_featured_image" => __( "Eliminar la imagen destacada de normativa", "hello-elementor" ),
		"use_featured_image" => __( "Usar como imagen destacada de normativa", "hello-elementor" ),
		"archives" => __( "Archivos de normativa", "hello-elementor" ),
		"insert_into_item" => __( "Insertar en normativa", "hello-elementor" ),
		"uploaded_to_this_item" => __( "Subir a normativa", "hello-elementor" ),
		"filter_items_list" => __( "Filtrar la lista de Normativa", "hello-elementor" ),
		"items_list_navigation" => __( "Navegación de la lista de Normativa", "hello-elementor" ),
		"items_list" => __( "Lista de Normativa", "hello-elementor" ),
		"attributes" => __( "Atributos de Normativa", "hello-elementor" ),
		"name_admin_bar" => __( "normativa", "hello-elementor" ),
		"item_published" => __( "normativa publicado", "hello-elementor" ),
		"item_published_privately" => __( "normativa publicado como privado.", "hello-elementor" ),
		"item_reverted_to_draft" => __( "normativa devuelto a borrador.", "hello-elementor" ),
		"item_scheduled" => __( "normativa programado", "hello-elementor" ),
		"item_updated" => __( "normativa actualizado.", "hello-elementor" ),
		"parent_item_colon" => __( "normativa superior", "hello-elementor" ),
	];

	$args = [
		"label" => __( "Normativa", "hello-elementor" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => "normativa",
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "normativa", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "custom-fields", "author" ],
		"taxonomies" => [ "categorias" ],
	];

	register_post_type( "normativa", $args );
}

add_action( 'init', 'cptui_register_my_cpts_normativa' );
function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Categorias.
	 */

	$labels = [
		"name" => __( "Categorias", "hello-elementor" ),
		"singular_name" => __( "Categoria", "hello-elementor" ),
		"menu_name" => __( "Categorias", "hello-elementor" ),
		"all_items" => __( "Todos los Categorias", "hello-elementor" ),
		"edit_item" => __( "Editar Categoria", "hello-elementor" ),
		"view_item" => __( "Ver Categoria", "hello-elementor" ),
		"update_item" => __( "Actualizar el nombre de Categoria", "hello-elementor" ),
		"add_new_item" => __( "Añadir nuevo Categoria", "hello-elementor" ),
		"new_item_name" => __( "Nombre del nuevo Categoria", "hello-elementor" ),
		"parent_item" => __( "Categoria superior", "hello-elementor" ),
		"parent_item_colon" => __( "Categoria superior", "hello-elementor" ),
		"search_items" => __( "Buscar Categorias", "hello-elementor" ),
		"popular_items" => __( "Categorias populares", "hello-elementor" ),
		"separate_items_with_commas" => __( "Separar Categorias con comas", "hello-elementor" ),
		"add_or_remove_items" => __( "Añadir o eliminar Categorias", "hello-elementor" ),
		"choose_from_most_used" => __( "Escoger entre los Categorias más usandos", "hello-elementor" ),
		"not_found" => __( "No se ha encontrado Categorias", "hello-elementor" ),
		"no_terms" => __( "Ningún Categorias", "hello-elementor" ),
		"items_list_navigation" => __( "Navegación de la lista de Categorias", "hello-elementor" ),
		"items_list" => __( "Lista de Categorias", "hello-elementor" ),
	];

	$args = [
		"label" => __( "Categorias", "hello-elementor" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'categorias', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "categorias",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		];
	register_taxonomy( "categorias", [ "normativa" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );
function cptui_register_my_taxes_categorias() {

	/**
	 * Taxonomy: Categorias.
	 */

	$labels = [
		"name" => __( "Categorias", "hello-elementor" ),
		"singular_name" => __( "Categoria", "hello-elementor" ),
		"menu_name" => __( "Categorias", "hello-elementor" ),
		"all_items" => __( "Todos los Categorias", "hello-elementor" ),
		"edit_item" => __( "Editar Categoria", "hello-elementor" ),
		"view_item" => __( "Ver Categoria", "hello-elementor" ),
		"update_item" => __( "Actualizar el nombre de Categoria", "hello-elementor" ),
		"add_new_item" => __( "Añadir nuevo Categoria", "hello-elementor" ),
		"new_item_name" => __( "Nombre del nuevo Categoria", "hello-elementor" ),
		"parent_item" => __( "Categoria superior", "hello-elementor" ),
		"parent_item_colon" => __( "Categoria superior", "hello-elementor" ),
		"search_items" => __( "Buscar Categorias", "hello-elementor" ),
		"popular_items" => __( "Categorias populares", "hello-elementor" ),
		"separate_items_with_commas" => __( "Separar Categorias con comas", "hello-elementor" ),
		"add_or_remove_items" => __( "Añadir o eliminar Categorias", "hello-elementor" ),
		"choose_from_most_used" => __( "Escoger entre los Categorias más usandos", "hello-elementor" ),
		"not_found" => __( "No se ha encontrado Categorias", "hello-elementor" ),
		"no_terms" => __( "Ningún Categorias", "hello-elementor" ),
		"items_list_navigation" => __( "Navegación de la lista de Categorias", "hello-elementor" ),
		"items_list" => __( "Lista de Categorias", "hello-elementor" ),
	];

	$args = [
		"label" => __( "Categorias", "hello-elementor" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'categorias', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "categorias",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		];
	register_taxonomy( "categorias", [ "normativa" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_categorias' );

