<?php
/**
 * The template for displaying singular post-types: posts, pages and user-defined custom post types.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php
while ( have_posts() ) : the_post();
	?>

<main <?php post_class( 'site-main' ); ?> role="main">
	<?php if ( apply_filters( 'hello_elementor_page_title', true ) ) : ?>
	
	<?php endif; ?>
	<div class="page-content">
	    <?php if(get_post_type()=='noticias_portal'):?>
        <div class="page-content" wfd-id="29">
				<div data-elementor-type="wp-post" data-elementor-id="1978" class="elementor elementor-1978" data-elementor-settings="[]" wfd-id="32">
			<div class="elementor-inner" wfd-id="33">
				<div class="elementor-section-wrap" wfd-id="34">
							<section class="elementor-element elementor-element-906a10b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="906a10b" data-element_type="section" wfd-id="167">
						<div class="elementor-container elementor-column-gap-default" wfd-id="168">
				<div class="elementor-row" wfd-id="169">
				<div class="elementor-element elementor-element-d306bbc elementor-column elementor-col-100 elementor-top-column" data-id="d306bbc" data-element_type="column" wfd-id="170">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="171">
					<div class="elementor-widget-wrap" wfd-id="172">
				<div class="elementor-element elementor-element-9d7fadb elementor-widget elementor-widget-image" data-id="9d7fadb" data-element_type="widget" data-widget_type="image.default" wfd-id="173">
				<div class="elementor-widget-container" wfd-id="174">
					<div class="elementor-image" wfd-id="175">
										<img width="1861" height="862" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/demo-IMG_ArticuloInterno.png" class="attachment-full size-full" alt="" srcset="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/demo-IMG_ArticuloInterno.png 1861w, http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/demo-IMG_ArticuloInterno-300x139.png 300w, http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/demo-IMG_ArticuloInterno-768x356.png 768w, http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/demo-IMG_ArticuloInterno-1024x474.png 1024w" sizes="(max-width: 1861px) 100vw, 1861px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-9e1db27 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="9e1db27" data-element_type="section" wfd-id="151">
						<div class="elementor-container elementor-column-gap-default" wfd-id="152">
				<div class="elementor-row" wfd-id="153">
				<div class="elementor-element elementor-element-fa58f5e elementor-column elementor-col-50 elementor-top-column" data-id="fa58f5e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="160">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="161">
					<div class="elementor-widget-wrap" wfd-id="162">
				<div class="elementor-element elementor-element-cee9141 elementor-widget elementor-widget-spacer" data-id="cee9141" data-element_type="widget" data-widget_type="spacer.default" wfd-id="163">
				<div class="elementor-widget-container" wfd-id="164">
					<div class="elementor-spacer" wfd-id="165">
			<div class="elementor-spacer-inner" wfd-id="166"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-597c242 elementor-column elementor-col-50 elementor-top-column" data-id="597c242" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="154">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="155">
					<div class="elementor-widget-wrap" wfd-id="156">
				<div class="elementor-element elementor-element-a31df63 elementor-widget elementor-widget-text-editor" data-id="a31df63" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="157">
				<div class="elementor-widget-container" wfd-id="158">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="159"><p>ESPACIO PARA EL PIE DE FOTO – El Ministerio de Ambiente y Desarrollo Sostenible, junto con la Secretaría de Ambiente de la Alcaldía
de Bucaramanga y el apoyo económico de la Agencia Presidencial. (250 caracteres – WorkSans Medium 25pt.)</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-867426d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="867426d" data-element_type="section" wfd-id="130">
						<div class="elementor-container elementor-column-gap-default" wfd-id="131">
				<div class="elementor-row" wfd-id="132">
				<div class="elementor-element elementor-element-75bdc3c elementor-column elementor-col-25 elementor-top-column" data-id="75bdc3c" data-element_type="column" wfd-id="145">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="146">
					<div class="elementor-widget-wrap" wfd-id="147">
				<div class="elementor-element elementor-element-2314746 elementor-absolute elementor-widget elementor-widget-image" data-id="2314746" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default" wfd-id="148">
				<div class="elementor-widget-container" wfd-id="149">
					<div class="elementor-image" wfd-id="150">
										<img width="123" height="92" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/bot-compartir1.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-b73ec36 elementor-column elementor-col-25 elementor-top-column" data-id="b73ec36" data-element_type="column" wfd-id="139">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="140">
					<div class="elementor-widget-wrap" wfd-id="141">
				<div class="elementor-element elementor-element-45acf51 elementor-absolute elementor-widget elementor-widget-image" data-id="45acf51" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default" wfd-id="142">
				<div class="elementor-widget-container" wfd-id="143">
					<div class="elementor-image" wfd-id="144">
										<img width="123" height="93" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/bot-reportar1-1.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-7ffb217 elementor-column elementor-col-25 elementor-top-column" data-id="7ffb217" data-element_type="column" wfd-id="136">
			<div class="elementor-column-wrap" wfd-id="137">
					<div class="elementor-widget-wrap" wfd-id="138">
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-914cdcb elementor-column elementor-col-25 elementor-top-column" data-id="914cdcb" data-element_type="column" wfd-id="133">
			<div class="elementor-column-wrap" wfd-id="134">
					<div class="elementor-widget-wrap" wfd-id="135">
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-334cc68 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="334cc68" data-element_type="section" wfd-id="35">
						<div class="elementor-container elementor-column-gap-default" wfd-id="36">
				<div class="elementor-row" wfd-id="37">
				<div class="elementor-element elementor-element-edb2b10 elementor-column elementor-col-50 elementor-top-column" data-id="edb2b10" data-element_type="column" wfd-id="118">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="119">
					<div class="elementor-widget-wrap" wfd-id="120">
				<div class="elementor-element elementor-element-c847e14 elementor-widget elementor-widget-text-editor" data-id="c847e14" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="127">
				<div class="elementor-widget-container" wfd-id="128">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="129"><p>Publicado por: <?php the_author(); ?> Fecha: <?php the_date(); ?></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-16f1ade elementor-widget elementor-widget-text-editor" data-id="16f1ade" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="124">
				<div class="elementor-widget-container" wfd-id="125">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="126"><p><?php the_content(); ?></p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-9e92613 elementor-column elementor-col-50 elementor-top-column" data-id="9e92613" data-element_type="column" wfd-id="38">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="39">
					<div class="elementor-widget-wrap" wfd-id="40">
				<section class="elementor-element elementor-element-cf0aaee elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="cf0aaee" data-element_type="section" wfd-id="104">
						<div class="elementor-container elementor-column-gap-default" wfd-id="105">
				<div class="elementor-row" wfd-id="106">
				<div class="elementor-element elementor-element-163befc elementor-column elementor-col-50 elementor-inner-column" data-id="163befc" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="112">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="113">
					<div class="elementor-widget-wrap" wfd-id="114">
				<div class="elementor-element elementor-element-3f5ec5c elementor-widget elementor-widget-image" data-id="3f5ec5c" data-element_type="widget" data-widget_type="image.default" wfd-id="115">
				<div class="elementor-widget-container" wfd-id="116">
					<div class="elementor-image" wfd-id="117">
										<img width="47" height="42" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/ico-consultasfrecuentes.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-1f45746 elementor-column elementor-col-50 elementor-inner-column" data-id="1f45746" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="107">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="108">
					<div class="elementor-widget-wrap" wfd-id="109">
				<div class="elementor-element elementor-element-b711472 elementor-widget elementor-widget-heading" data-id="b711472" data-element_type="widget" data-widget_type="heading.default" wfd-id="110">
				<div class="elementor-widget-container" wfd-id="111">
			<h2 class="elementor-heading-title elementor-size-default">Noticias relacionadas</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-638d54d elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="638d54d" data-element_type="section" wfd-id="83">
						<div class="elementor-container elementor-column-gap-default" wfd-id="84">
				<div class="elementor-row" wfd-id="85">
				<div class="elementor-element elementor-element-2653705 elementor-column elementor-col-50 elementor-inner-column" data-id="2653705" data-element_type="column" wfd-id="98">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="99">
					<div class="elementor-widget-wrap" wfd-id="100">
				<div class="elementor-element elementor-element-ffc94b8 elementor-widget elementor-widget-image" data-id="ffc94b8" data-element_type="widget" data-widget_type="image.default" wfd-id="101">
				<div class="elementor-widget-container" wfd-id="102">
					<div class="elementor-image" wfd-id="103">
										<img width="169" height="199" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-1855ccb elementor-column elementor-col-50 elementor-inner-column" data-id="1855ccb" data-element_type="column" wfd-id="86">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="87">
					<div class="elementor-widget-wrap" wfd-id="88">
				<div class="elementor-element elementor-element-ed0884d elementor-widget elementor-widget-text-editor" data-id="ed0884d" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="92">
				<div class="elementor-widget-container" wfd-id="93">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="94"><p><span style="color: #3366cc;" wfd-id="97">24 / 09 / 2019</span></p><p><strong><span style="text-decoration: underline; color: #3366cc;" wfd-id="96">ESPACIO PARA TÍTULO</span></strong><br><strong><span style="text-decoration: underline; color: #3366cc;" wfd-id="95">CORTO DE LA NOTICIA</span></strong></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-2919ceb elementor-widget elementor-widget-text-editor" data-id="2919ceb" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="89">
				<div class="elementor-widget-container" wfd-id="90">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="91"><p>Categoría:<br>Dirección de Bosques Biodiversidad<br>y Servicios Ecosistémicos.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-b99d113 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="b99d113" data-element_type="section" wfd-id="62">
						<div class="elementor-container elementor-column-gap-default" wfd-id="63">
				<div class="elementor-row" wfd-id="64">
				<div class="elementor-element elementor-element-fac0369 elementor-column elementor-col-50 elementor-inner-column" data-id="fac0369" data-element_type="column" wfd-id="77">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="78">
					<div class="elementor-widget-wrap" wfd-id="79">
				<div class="elementor-element elementor-element-7800fa7 elementor-widget elementor-widget-image" data-id="7800fa7" data-element_type="widget" data-widget_type="image.default" wfd-id="80">
				<div class="elementor-widget-container" wfd-id="81">
					<div class="elementor-image" wfd-id="82">
										<img width="169" height="199" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-165ead2 elementor-column elementor-col-50 elementor-inner-column" data-id="165ead2" data-element_type="column" wfd-id="65">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="66">
					<div class="elementor-widget-wrap" wfd-id="67">
				<div class="elementor-element elementor-element-5f45346 elementor-widget elementor-widget-text-editor" data-id="5f45346" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="71">
				<div class="elementor-widget-container" wfd-id="72">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="73"><p><span style="color: #3366cc;" wfd-id="76">24 / 09 / 2019</span></p><p><strong><span style="text-decoration: underline; color: #3366cc;" wfd-id="75">ESPACIO PARA TÍTULO</span></strong><br><strong><span style="text-decoration: underline; color: #3366cc;" wfd-id="74">CORTO DE LA NOTICIA</span></strong></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-942b1bb elementor-widget elementor-widget-text-editor" data-id="942b1bb" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="68">
				<div class="elementor-widget-container" wfd-id="69">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="70"><p>Categoría:<br>Dirección de Bosques Biodiversidad<br>y Servicios Ecosistémicos.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-3791910 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="3791910" data-element_type="section" wfd-id="41">
						<div class="elementor-container elementor-column-gap-default" wfd-id="42">
				<div class="elementor-row" wfd-id="43">
				<div class="elementor-element elementor-element-b47d674 elementor-column elementor-col-50 elementor-inner-column" data-id="b47d674" data-element_type="column" wfd-id="56">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="57">
					<div class="elementor-widget-wrap" wfd-id="58">
				<div class="elementor-element elementor-element-d129d0a elementor-widget elementor-widget-image" data-id="d129d0a" data-element_type="widget" data-widget_type="image.default" wfd-id="59">
				<div class="elementor-widget-container" wfd-id="60">
					<div class="elementor-image" wfd-id="61">
										<img width="169" height="199" src="http://158.69.170.226/~portalmads/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-ecc3731 elementor-column elementor-col-50 elementor-inner-column" data-id="ecc3731" data-element_type="column" wfd-id="44">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="45">
					<div class="elementor-widget-wrap" wfd-id="46">
				<div class="elementor-element elementor-element-05935c7 elementor-widget elementor-widget-text-editor" data-id="05935c7" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="50">
				<div class="elementor-widget-container" wfd-id="51">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="52"><p><span style="color: #3366cc;" wfd-id="55">24 / 09 / 2019</span></p><p><strong><span style="text-decoration: underline; color: #3366cc;" wfd-id="54">ESPACIO PARA TÍTULO</span></strong><br><strong><span style="text-decoration: underline; color: #3366cc;" wfd-id="53">CORTO DE LA NOTICIA</span></strong></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-2eddd58 elementor-widget elementor-widget-text-editor" data-id="2eddd58" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="47">
				<div class="elementor-widget-container" wfd-id="48">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="49"><p>Categoría:<br>Dirección de Bosques Biodiversidad<br>y Servicios Ecosistémicos.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
		<p></p>		<div wfd-id="31">
                        </div>
		<div class="post-tags" wfd-id="30">
					</div>
			</div>
        <?php endif;?>
        
         <?php if(get_post_type()=='page'):?> 
        <div>
            <?php the_content(); 
			echo'<p>';
			the_field('documentos');
		    echo '</p>';
		    ?>
        </div>
        <?php endif;?>
        
		
		
		<div class="post-tags">
			<?php the_tags( '<span class="tag-links">' . __( 'Tagged ', 'hello-elementor' ), null, '</span>' ); ?>
		</div>
		<?php wp_link_pages(); ?>
	</div>

	<?php comments_template(); ?>
</main>

	<?php
endwhile;
